<?php
require_once("functions.php");
$validation = new Validation();

$error = "";
if(isset($_POST["submit"])){
	$title = $_POST["title"];
	$content = $_POST["content"];

	//バリデーション
	$validation->checkEmpty($title, "タイトル");
	$validation->checkMaxLength($title, "タイトル", 10);
	$validation->checkEmpty($content, "本文");
	$validation->checkMaxLength($content, "本文", 40);

	//$errorに追加されたエラー文字列を格納
	$error = $validation->getError();
	
	if(!$error){
		$pdo = connect_db();
		//SQLインジェクション対策
		$st = $pdo->prepare("INSERT INTO `post` (`title`, `content`) VALUES (?, ?)");
		$st->execute(array($title, $content));

		//リダイレクト
		header('Location: index.php');
		exit;
	}
}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>ブログ</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

	<main class="blog">

		<form action="post.php" method="post">
			<section class="article">
				<h2 class="article__title">記事投稿</h2>
				
				<div class="article__post">
					<div class="article__postSet">
						<p>題名</p>
						<p><input type="text" name="title" size="40"></p>
					</div>

					<div class="article__postSet">
						<p>本文</p>
						<p><textarea name="content" row="8" cols="40"></textarea></p>
					</div>

					<div class="article__postSet">
						<p><input class="article__submit" name="submit" type="submit" value="投稿"></p>
						<?php if($error): ?>
		    			<p class="article__error">
		    				<?php echo $error; ?>
						</p>
						<?php endif; ?>
		    		</div>
		    	</div>
			</section>
		</form>
	</main>

</body>
</html>