<?php

require_once("functions.php");

//記事投稿のデータを取得し配列に格納
$pdo = connect_db();
$st = $pdo->query("SELECT * FROM `post` ORDER BY `id` DESC");
$posts = $st->fetchAll();

//コメントのデータを取得し$postのキーが"comments"の配列に格納
for($i=0; $i<count($posts); $i++){
    $id = $posts[$i]["id"];
    $pdo = connect_db();
    $st = $pdo->query("SELECT * FROM `comment` WHERE `post_id`=$id ORDER BY `id` DESC");
    $comments = $st->fetchAll();
    $posts[$i]["comments"] = $comments;
}

//テンプレートを読み込む
include("tmpl/header.tmpl");
include("tmpl/top.tmpl");
include("tmpl/footer.tmpl"); 

?>