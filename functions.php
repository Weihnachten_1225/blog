<?php
ini_set('display_errors', 'On');

//Validationクラスを作成
class Validation{
	//アクセス修飾子
	private $error = "";

	public function checkEmpty($text, $name){
		if(!$text){
			$this->error .= $name . "が入力されていません。<br>";
		}
	}
	public function checkMaxLength($text, $name, $maxLength){
		if(strlen($text)>$maxLength){
			$this->error .= $name . "が長すぎます。<br>
			文字列は半角英数字で".$maxLength."文字未満でお願いします。<br>";
		}
	}
	//ゲッター(メソッド)：privateに設定したプロパティを取得するための関数
	public function getError(){
		return $this->error;
	}
}

const DB_NAME = "blog";
const DB_ACCOUNT = "root";

function connect_db(){
    $pdo = new PDO("mysql:dbname=".DB_NAME, DB_ACCOUNT);
    return $pdo;
}

function v($data){
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
}

function datetime_format($time){
    $date = new DateTime($time);
    return $date->format("Y年m月d日 H:i");
}

?>